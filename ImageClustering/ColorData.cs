﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageClustering
{
    public class ColorData
    {
        public byte A, R, G, B;
        public int Group = -1;

        public ColorData()
        {
        }

        public ColorData(Color color)
        {
            A = color.A;
            R = color.R;
            G = color.G;
            B = color.B;
            Group = -1;
        }

        public ColorData(ColorData color)
        {
            A = color.A;
            R = color.R;
            G = color.G;
            B = color.B;
            Group = color.Group;
        }

        public ColorData(ColorData[] colors, int fallbackGroup)
        {
            int r = 0;
            int g = 0;
            int b = 0;

            for (int i = 0; i < colors.Length; i++)
            {
                r += colors[i].R;
                g += colors[i].G;
                b += colors[i].B;
            }

            if(colors.Length == 0)
            {
                R = 0;
                G = 0;
                B = 0;
                Group = fallbackGroup;
            }
            else
            { 
                R = (byte)(r / colors.Length);
                G = (byte)(g / colors.Length);
                B = (byte)(b / colors.Length);
                Group = colors[0].Group;
            }
        }

        public static double Distance(ColorData a, ColorData b)
        {
            double deltaR = a.R - b.R;
            double deltaG = a.G - b.G;
            double deltaB = a.B - b.B;

            return Math.Sqrt(Math.Pow(deltaR, 2) + Math.Pow(deltaG, 2) + Math.Pow(deltaB, 2));
        }
    }

    public static class ColorDataExtenstions
    {
        public static int Width(this ColorData[,] bmp)
        {
            return bmp.GetLength(0);
        }

        public static int Height(this ColorData[,] bmp)
        {
            return bmp.GetLength(1);
        }

        public static Bitmap ToBitmap(this ColorData[,] bmp)
        {
            Bitmap result = new Bitmap(bmp.GetLength(0), bmp.GetLength(1));

            for(int x = 0; x < bmp.GetLength(0); x++)
            {
                for(int y = 0; y < bmp.GetLength(1); y++)
                {
                    ColorData color = bmp[x, y];

                    result.SetPixel(x, y, Color.FromArgb(color.A, color.R, color.G, color.B));
                }
            }

            return result;
        }

        public static ColorData[,] ToColorDataMatrix(this Bitmap bmp)
        {
            ColorData[,] result = new ColorData[bmp.Width, bmp.Height];

            for(int x = 0; x < bmp.Width; x++)
            {
                for(int y = 0; y < bmp.Height; y++)
                {
                    Color color = bmp.GetPixel(x, y);

                    result[x, y] = new ColorData(color);
                    result[x, y].Group = -1;
                }
            }

            return result;
        }

        public static ColorData[] GetByGroup(this ColorData[,] bmp, int group)
        {
            List<ColorData> colorsList = new List<ColorData>();

            for(int x = 0; x < bmp.Width(); x++)
            {
                for(int y = 0; y < bmp.Height(); y++)
                {
                    if(bmp[x, y].Group == group)
                        colorsList.Add(bmp[x, y]);
                }
            }

            return colorsList.ToArray();
        }
    }
}
