﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using System.Drawing.Imaging;
using ImageClustering;

namespace ImageClustering
{
    public partial class Form1 : Form
    {
        private int progressBarValue = 0;

        public int ProgressBarValue
        {
            get
            {
                return progressBarValue;
            }

            set
            {
                if(value > progressBarValue)
                    progressBarValue = value;
            }
        }

        public Form1()
        {
            InitializeComponent();

            button2.Visible = false;
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";
            DialogResult result = openFileDialog1.ShowDialog();

            if(result != DialogResult.OK)
                return;

            selectedFilePath.Text = openFileDialog1.FileName;

            button2.Visible = true;
        }

        private void text_changed(object sender, EventArgs e)
        {
            amountOfClusters.Text = Regex.Match(amountOfClusters.Text, "[0-9]+").ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            button2.Visible = false;

            Bitmap img = new Bitmap(Bitmap.FromFile(openFileDialog1.FileName));

            if(preBlurCheckbox.Checked)
            {
                img = Blur(img);
            }

            Bitmap resultImg = null;

            //Thread thread = new Thread(() => 
            Clusterize(img, out resultImg);
            /*
            thread.Start();

            while (thread.IsAlive)
            {
                progressBar1.Value = ProgressBarValue;
            }*/

            saveFileDialog1.Filter = "PNG Image|*.png|JPeg Image|*.jpg|Bitmap Image|*.bmp|Gif Image|*.gif";
            DialogResult result = saveFileDialog1.ShowDialog();

            if(result != DialogResult.OK)
                return;

            Stream stream = saveFileDialog1.OpenFile();

            saveFileDialog1.AddExtension = true;

            switch(saveFileDialog1.FilterIndex)
            {
                case 1:
                    resultImg.Save(stream, ImageFormat.Png);
                    break;

                case 2:
                    resultImg.Save(stream, ImageFormat.Jpeg);
                    break;

                case 3:
                    resultImg.Save(stream, ImageFormat.Bmp);
                    break;

                case 4:
                    resultImg.Save(stream, ImageFormat.Gif);
                    break;
            }

            stream.Close();

            progressBarValue = 0;
        }

        private Bitmap Blur (Bitmap bmp)
        {
            ColorData[,] result = bmp.ToColorDataMatrix();
            ColorData[,] original = bmp.ToColorDataMatrix();

            int size = (int)Math.Ceiling(((bmp.Width + bmp.Height) / 2) * 0.01);

            if (size > 4)
                size = 4;

            int w = bmp.Width;
            int h = bmp.Height;

            Parallel.For(0, result.Width(), x =>
            {
                Parallel.For(0, result.Height(), y =>
                {
                    int avgR = 0;
                    int avgG = 0;
                    int avgB = 0;
                    int avgA = 0;
                    int amt = 0;

                    for(int kX = -size; kX < size; kX ++)
                    {
                        for(int kY = -size; kY < size; kY++)
                        {
                            int aX = x + kX;
                            int aY = y + kY;

                            if (aX >= 0 && aX < w && aY >= 0 && aY < h)
                            {
                                avgR += original[aX, aY].R;
                                avgG += original[aX, aY].G;
                                avgB += original[aX, aY].B;
                                avgA += original[aX, aY].A;

                                amt++;
                            }
                        }
                    }

                    avgR /= amt;
                    avgG /= amt;
                    avgB /= amt;
                    avgA /= amt;

                    result[x, y].A = (byte)avgA;
                    result[x, y].R = (byte)avgR;
                    result[x, y].G = (byte)avgG;
                    result[x, y].B = (byte)avgB;
                });
            });

            return result.ToBitmap();
        }

        private void Clusterize(Bitmap bmp, out Bitmap result)
        {   
            ColorData[,] img = bmp.ToColorDataMatrix();

            bool progressBarSkippedFirstStep = false;

            int clusterAmount = 5;

            int.TryParse(amountOfClusters.Text, out clusterAmount);

            ColorData[] clusterCentroids = new ColorData[clusterAmount];

            for(int i = 0; i < clusterAmount; i++)
            {
                clusterCentroids[i] = new ColorData();

                clusterCentroids[i].Group = i;
            }

            int initialGroupsAssigned = 0;

            Random rand = new Random();

            while(initialGroupsAssigned < clusterAmount)
            {
                int randX = rand.Next(img.Width());
                int randY = rand.Next(img.Height());

                if(img[randX, randY].Group < 0) //unassigned is -1.
                {
                    img[randX, randY].Group = initialGroupsAssigned;
                    initialGroupsAssigned++;
                }
            }

            for(int i = 0; i < clusterAmount; i++)
            {
                clusterCentroids[i] = new ColorData(img.GetByGroup(i), clusterCentroids[i].Group);
            }

            int changes = 1;
            int highestChange = 0;

            while(changes > 0)
            {
                changes = 0;

                for(int i = 0; i < clusterAmount; i++)
                {
                    clusterCentroids[i] = new ColorData(img.GetByGroup(i), clusterCentroids[i].Group);
                }

                Parallel.For(0, img.Width(), x =>
                {
                    Parallel.For(0, img.Height(), y =>
                    {
                        double dist = double.MaxValue;
                        int group = -1;

                        for(int i = 0; i < clusterCentroids.Length; i++)
                        {
                            double currDist = ColorData.Distance(img[x, y], clusterCentroids[i]);

                            if(currDist < dist || group == -1)
                            {
                                dist = currDist;
                                group = clusterCentroids[i].Group;
                            }
                        }

                        if (img[x, y].Group != group)
                        {
                            img[x, y].Group = group;
                            changes++;
                        }
                    });
                });

                if (progressBarSkippedFirstStep)
                {
                    if (changes > highestChange)
                        highestChange = changes;

                    ProgressBarValue = (int)((1 - ((double)changes / (double)highestChange)) * 100f);

                    progressBar1.Value = ProgressBarValue;

                    progressBar1.Invalidate();
                    progressBar1.Update();
                    progressBar1.Refresh();
                    Application.DoEvents();

                    Console.WriteLine(ProgressBarValue);
                }
                else
                {
                    progressBarSkippedFirstStep = true;
                }
            }

            Parallel.For(0, img.Width(), x =>
            {
                Parallel.For(0, img.Height(), y =>
                {
                    byte a = img[x, y].A;

                    if(img[x, y].Group >= 0)
                    {
                        img[x, y] = new ColorData(clusterCentroids[img[x, y].Group]);
                    }
                    else
                    {
                        img[x, y].G = 0;
                        
                        img[x, y].R = (byte)(Math.Sin((x + y) / 3) * 255);
                        img[x, y].B = (byte)(Math.Sin((x + y) / 3) * 255);
                    }

                    img[x, y].A = a;
                });
            });

            result = img.ToBitmap();
            
            progressBar1.Value = 0;
            progressBarValue = 0;
            selectedFilePath.Text = "";
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://www.timfalken.com");
        }
    }
}
